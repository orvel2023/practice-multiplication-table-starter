package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(isValid(start,end)) {
            return generateTable(start,end);
        } else {
            return null;
        }
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end) ;
    }

    public Boolean isInRange(int number) {
        return number <= 1000 && number >=1;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        String table = new String();
        for (int i = start; i <= end; i++) {

            if (i!=end){
                table += generateLine(start,i) + "%n";
            } else {
                table += generateLine(start,i);
            }
        }
        return String.format(table);
    }

    public String generateLine(int start, int row) {
        String line = new String();
        for (int i = start; i <= row; i++) {
           line += generateSingleExpression(i,row) + "  ";
        }
        line = line.trim();
        return line;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier + "=" + multiplicand * multiplier;
    }
}
